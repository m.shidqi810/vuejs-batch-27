// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var text1 = pertama.substr(0,19);
var text2 = kedua.substr(0,8);
var text3 = kedua.substr(8,10).toUpperCase();
console.log(text1.concat(text2.concat(text3) ) );

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angka1 = Number(kataPertama);
var angka2 = Number(kataKedua);
var angka3 = Number(kataKetiga);
var angka4 = Number(kataKeempat);

var hasil = angka1 + (angka2*angka3) + angka4;

console.log(hasil);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);