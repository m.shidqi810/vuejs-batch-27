// soal 1
var nilai = 90 
if ( nilai >= 85 ) {
    console.log("Nilai nya A")
}
else if (nilai >= 75 && nilai < 85) {
    console.log("Nilai nya B")
}
else if (nilai >= 65 && nilai < 75) {
    console.log("Nilai nya C")
}
else if (nilai >= 55 && nilai < 65) {
    console.log("Nilai nya D")
}
else if (nilai < 55) {
    console.log("Nilai nya E")
}
console.log("")


// soal 2
var tanggal = 08
var bulan = 10
var tahun = 2000

switch(bulan) {
    case 1:   { console.log(tanggal + ' Januari ' + tahun); break; }
    case 2:   { console.log(tanggal + ' Februari ' + tahun); break; }
    case 3:   { console.log(tanggal + ' Maret ' + tahun); break; }
    case 4:   { console.log(tanggal + ' April ' + tahun); break; }
    case 5:   { console.log(tanggal + ' Mei ' + tahun); break; }
    case 6:   { console.log(tanggal + ' Juni ' + tahun); break; }
    case 7:   { console.log(tanggal + ' Juli ' + tahun); break; }
    case 8:   { console.log(tanggal + ' Agustus ' + tahun); break; }
    case 9:   { console.log(tanggal + ' September ' + tahun); break; }
    case 10:   { console.log(tanggal + ' Oktober ' + tahun); break; }
    case 11:   { console.log(tanggal + ' November ' + tahun); break; }
    case 12:   { console.log(tanggal + ' Desember ' + tahun); break; }
    default:  { console.log('Bulan hanya 1 - 12 heii!!!!'); }
}
console.log("")

// soal3
var n = 7
var hasil = ""
for(var i = 0; i <= n; i++) {
  for (var j = 0; j <= i; j++) {
    hasil += "# "
  }
  hasil += "\n"
}

console.log(hasil)


// soal 4
var m = 10
var kata = ""
var satu, dua, tiga, j
var garis = ""
for (var i = 1; i <= m; i++) {
    if (i % 3 == 1 ) {
        console.log(i + " - I love programming")
    }
    if (i % 3 == 2 ) {
        console.log(i + " - I love Javascript")
    }
    if (i % 3 == 0 ) {
        console.log(i + " - I love Vue Js")
        for (j = 1; j <= 3; j++) {
            garis += "="
        }
        console.log(garis)
    }
}