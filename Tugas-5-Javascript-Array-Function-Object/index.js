// soal 1
console.log("Nomor 1")
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort()
for (let i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i])
}

console.log("")

// soal 2
console.log("Nomor 2")
function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + " Jalan Pelesiran, dan saya punya hobby yaitu " + data.hobby
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan)

console.log("")

// soal 3
console.log("Nomor 3")
function hitung_huruf_vokal(kata) {
    var jumlahVokal = 0
    for (let j = 0; j < kata.length; j++) {
        if (kata.substring(j, j+1) == 'a' || kata.substring(j, j+1) == 'i' || kata.substring(j, j+1) == 'u'
        || kata.substring(j, j+1) == 'e' || kata.substring(j, j+1) == 'o' ||
        kata.substring(j, j+1) == 'A' || kata.substring(j, j+1) == 'I' || kata.substring(j, j+1) == 'U'
        || kata.substring(j, j+1) == 'E' || kata.substring(j, j+1) == 'O'
        ) {
            jumlahVokal = jumlahVokal + 1
        }
    }
    return jumlahVokal
}

var hitung_1 = jumlah_kata("Muhammad")

var hitung_2 = jumlah_kata("Iqbal")

console.log(hitung_1 , hitung_2)

console.log("")

// soal 4
console.log("Nomor 4")
function hitung(angka) {
    return (angka * 2) - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8