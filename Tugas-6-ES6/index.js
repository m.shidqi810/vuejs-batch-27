// soal 1
const luasPersegiPanjang = (panjang, lebar) => {
    let luas = panjang * lebar
    return luas
}

console.log(luasPersegiPanjang(10,5))

const kelilingPersegiPanjang = (panjang, lebar) => {
    let luas = 2 * (panjang + lebar)
    return luas
}

console.log(kelilingPersegiPanjang(10,5))


// soal 2
const literal = (nama1, nama2) => {
    return {
        firstName: nama1, //variabel ini tidak saya pakai, namun tidak di hapus krn takut salah
        lastName: nama2, //variabel ini tidak saya pakai, namun tidak di hapus krn takut salah
        namaLengkap: () => {
            console.log(`${nama1} ${nama2}`)
        }
    }
}

literal("Budi", "sanjaya").namaLengkap()


// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football"
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)


// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
//Driver Code
console.log(combined)


// soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet
let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)
console.log(after)