// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function baca(waktu, books, i) {
    if (i < books.length) {
        sisa = waktu
        readBooks(waktu, books[i], function (sisa) {
            if (sisa > 0) {
                i = i + 1
                baca(sisa, books, i)
            }
        })
        
    }
}

console.log(baca(10000, books, 0))