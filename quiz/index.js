function jumlah_kata(kata) {
    var kataBaru = kata.trim()
    var jumlahnya = kataBaru.split(" ").length
    return jumlahnya
}
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"


var jumlah1 = jumlah_kata(kalimat_1) // 6
var jumlah2 = jumlah_kata(kalimat_2) // 2

console.log(jumlah1)
console.log(jumlah2)


// soal 2
var tanggal = 31
var bulan = 12
var tahun = 2020
function next_date(bulan, tanggal, tahun) {
    var tanggalBesok, bulanBesok, tanggalBesok
    var today = new Date(bulan + '/' + tanggal + '/' + tahun);
    var tomorrow = new Date(today);
    tomorrow.setDate(today.getDate()+1);
    var besok = tomorrow.toLocaleDateString();
    
    // untuk bulan 2 digit, tanggal 1 digit
    if (besok.substring(2,3) == '/' && besok.substring(4,5) == '/') {
        bulanBesok = Number(besok.substring(0,2))
        tanggalBesok = Number(besok.substring(3,4))
        tahunBesok = Number(besok.substring(5,9))
    }

    // untuk bulan 2 digit, tanggal 2 digit
    else if (besok.substring(2,3) == '/' && besok.substring(5,6) == '/') {
        bulanBesok = Number(besok.substring(0,2))
        tanggalBesok = Number(besok.substring(3,5))
        tahunBesok = Number(besok.substr(6,10))
    }

    // untuk bulan 1 digit, tanggal 1 digit
    else if (besok.substring(1,2) == '/' && besok.substring(3,4) == '/') {
        bulanBesok = Number(besok.substring(0,1))
        tanggalBesok = Number(besok.substring(2,3))
        tahunBesok = Number(besok.substr(4,8))
    }

    // untuk bulan 1 digit, tanggal 2 digit
    else if (besok.substring(1,2) == '/' && besok.substring(4,5) == '/') {
        bulanBesok = Number(besok.substring(0,1))
        tanggalBesok = Number(besok.substring(2,4))
        tahunBesok = Number(besok.substr(5,9))
    }

    switch(bulanBesok) {
        case 1:   { return tanggalBesok + ' Januari ' + tahunBesok; break; }
        case 2:   { return tanggalBesok + ' Februari ' + tahunBesok; break; }
        case 3:   { return tanggalBesok + ' Maret ' + tahunBesok; break; }
        case 4:   { return tanggalBesok + ' April ' + tahunBesok; break; }
        case 5:   { return tanggalBesok + ' Mei ' + tahunBesok; break; }
        case 6:   { return tanggalBesok + ' Juni ' + tahunBesok; break; }
        case 7:   { return tanggalBesok + ' Juli ' + tahunBesok; break; }
        case 8:   { return tanggalBesok + ' Agustus ' + tahunBesok; break; }
        case 9:   { return tanggalBesok + ' September ' + tahunBesok; break; }
        case 10:   { return tanggalBesok + ' Oktober ' + tahunBesok; break; }
        case 11:   { return tanggalBesok + ' November ' + tahunBesok; break; }
        case 12:   { return tanggalBesok + ' Desember ' + tahunBesok; break; }
        default:  { return 'Bulan hanya 1 - 12 heii!!!!'; }
    }
}

console.log(next_date(bulan , tanggal , tahun))